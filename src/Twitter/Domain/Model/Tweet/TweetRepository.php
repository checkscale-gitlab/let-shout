<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 12:29
 */

namespace App\Twitter\Domain\Model\Tweet;


interface TweetRepository
{
    /**
     * @param TweetUsername $username
     * @param int $total
     * @return array|null
     */
    public function tweetsByUsername(TweetUsername $username, int $total);

}