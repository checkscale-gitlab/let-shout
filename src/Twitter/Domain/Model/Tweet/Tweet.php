<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 12:03
 */

namespace App\Twitter\Domain\Model\Tweet;

use DateTimeImmutable;

/**
 * Class Tweet
 * @package App\Twitter\Domain\Model\Tweet
 */
class Tweet
{
    private $id;
    private $text;
    private $createdAt;
    private $username;
    private $usernameImage;

    /**
     * Tweet constructor.
     * @param TweetId $id
     * @param TweetText $text
     * @param TweetUsername $username
     * @param TweetUserImage $usernameImage
     * @param DateTimeImmutable $createdAt
     */
    private function __construct(
        TweetId $id,
        TweetText $text,
        TweetUsername $username,
        TweetUserImage $usernameImage,
        DateTimeImmutable $createdAt = null
    ){
        $this->id = $id;
        $this->text = $text;
        $this->username = $username;
        $this->usernameImage = $usernameImage;
        $this->createdAt = $createdAt ?? new DateTimeImmutable();
    }

    public static function populateFromArray(array $data)
    {
        return new self(
            new TweetId($data['id']),
            new TweetText($data['text']),
            new TweetUsername($data['user']['screen_name']),
            new TweetUserImage($data['user']['profile_image_url']),
            DateTimeImmutable::createFromFormat('D M j H:i:s P Y',$data['created_at'])
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function getText(): string
    {
        return sprintf('¡%s!', strtoupper($this->text));
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUsernameImage(): string
    {
        return $this->usernameImage;
    }
}