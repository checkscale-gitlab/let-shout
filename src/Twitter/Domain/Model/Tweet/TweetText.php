<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/12/17
 * Time: 22:33
 */

namespace App\Twitter\Domain\Model\Tweet;

/**
 * Class TweetText
 * @package App\Twitter\Domain\Model\Tweet
 */
class TweetText
{
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 280;

    /**
     * @var string
     */
    private $value;

    /**
     * TweetText constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->setValue($text);
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function getValue()
    {
        return $this->value;
    }

    private function setValue(string $text)
    {
        $this->assertNotEmpty($text);
        $this->assertFitsLength($text);
        $this->value = trim($text);
    }

    private function assertNotEmpty($text)
    {
        if (empty($text)) {
            throw new \DomainException('Text must not be empty.');
        }
    }

    private function assertFitsLength($text)
    {
        if (strlen($text) < self::MIN_LENGTH) {
            throw new \DomainException('Text is too sort');
        }
        if (strlen($text) > self::MAX_LENGTH) {
            throw new \DomainException('Text is too long');
        }
    }
}