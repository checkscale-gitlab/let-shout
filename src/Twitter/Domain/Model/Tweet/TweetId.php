<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:39
 */

namespace App\Twitter\Domain\Model\Tweet;

use App\Common\Domain\Model\ValueObjects\UuId;

/**
 * Class TweetId
 * @package App\Twitter\Domain\Model\User
 */
final class TweetId
{
    private $value;

    /**
     * TweetId constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->setValue($id);
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function getValue()
    {
        return $this->value;
    }

    private function setValue(string $id)
    {
        $this->assertNotEmpty($id);
        $this->assertIsNumber($id);
        $this->value = $id;
    }

    private function assertNotEmpty($id)
    {
        if (empty($id)) {
            throw new \DomainException('Id must not be empty.');
        }
    }

    private function assertIsNumber($id)
    {
        if (!is_numeric($id)) {
            throw new \DomainException('Id must be a number.');
        }
    }
}