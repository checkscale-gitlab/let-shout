<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 19:56
 */

namespace App\Twitter\Infrastructure\Controller;

use App\Twitter\Application\Query\Tweet\TweetShoutQuery;
use App\Twitter\Application\Query\Tweet\TweetShoutQueryHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PagesController
 * @package App\Twitter\Infrastructure\Controller
 */
class TweetsController extends Controller
{
    /**
     * @var TweetShoutQueryHandler
     */
    private $queryHandler;

    /**
     * TweetsController constructor.
     * @param TweetShoutQueryHandler $commandHandler
     */
    public function __construct(TweetShoutQueryHandler $commandHandler)
    {
        $this->queryHandler = $commandHandler;
    }

    public function homepage()
    {
        return $this->render('tweets/homepage.html.twig');
    }

    public function tweetByUser(Request $request): JsonResponse
    {
        try {
            $query = new TweetShoutQuery(
                $request->get('username', 'demo'),
                $request->get('limit', 1)
            );
            $result = $this->queryHandler->handle($query);
            $status = 200;
        } catch (\Exception $e) {
            $result = [];
            $status = 404;
        }

        return $this->json($result, $status);
    }
}