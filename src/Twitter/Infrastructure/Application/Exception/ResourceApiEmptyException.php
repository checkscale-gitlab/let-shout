<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 15:07
 */

namespace App\Twitter\Infrastructure\Application\Exception;


use Throwable;

class ResourceApiEmptyException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}