<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 14:14
 */

namespace App\Twitter\Infrastructure\Application\Service;


interface ApiExchange
{
    public function getSettings(): array;
    public function getUrlBase(): string;
}