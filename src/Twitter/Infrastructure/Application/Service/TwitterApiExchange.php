<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 14:12
 */

namespace App\Twitter\Infrastructure\Application\Service;

use App\Twitter\Infrastructure\Application\Exception\ResourceApiEmptyException;
use TwitterAPIExchange as BaseExchange;

class TwitterApiExchange extends BaseExchange
{
    const POST = 'POST';
    const GET = 'GET';
    /**
     * @var ApiExchange
     */
    protected $exchange;

    public function __construct(ApiExchange $exchange)
    {
        $this->exchange = $exchange;
        parent::__construct($this->exchange->getSettings());
    }

    /**
     * @param string $resource
     * @param string $requestMethod
     * @return BaseExchange
     * @throws ResourceApiEmptyException
     * @throws \Exception
     */
    public function buildOauth($resource, $requestMethod)
    {
        $this->assertResourceIsNotEmpty($resource);

        $url = $this->exchange->getUrlBase() . $resource;
        return parent::buildOauth($url, $requestMethod);
    }

    /**
     * @param $resource
     * @throws ResourceApiEmptyException
     */
    private function assertResourceIsNotEmpty($resource)
    {
        if(empty($resource)){
            throw new ResourceApiEmptyException('The api resource can not be empty');
        }
    }
}