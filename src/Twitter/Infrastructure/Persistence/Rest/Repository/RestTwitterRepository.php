<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/01/18
 * Time: 14:46
 */

namespace App\Twitter\Infrastructure\Persistence\Rest\Repository;

use App\Twitter\Application\Exception\UsernameNotFoundException;
use App\Twitter\Domain\Model\Tweet\Tweet;
use App\Twitter\Domain\Model\Tweet\TweetRepository;
use App\Twitter\Domain\Model\Tweet\TweetUsername;
use App\Twitter\Infrastructure\Application\Service\TwitterApiExchange;

class RestTwitterRepository implements TweetRepository
{
    /**
     * @var TwitterApiExchange
     */
    private $exchange;

    public function __construct(TwitterApiExchange $exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * @param TweetUsername $username
     * @param int $total
     * @return Tweet[]
     * @throws \Exception
     */
    public function tweetsByUsername(TweetUsername $username, int $total): array
    {
        $resource = 'statuses/user_timeline.json';
        $getfield = sprintf('?screen_name=%s&count=%d', $username->getValue(), $total);

        try{
            $result = $this->exchange
                ->setGetfield($getfield)
                ->buildOauth($resource, TwitterApiExchange::GET)
                ->performRequest();

            $tweets = json_decode($result,  true, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            $this->assertUsernameNotFound($tweets);

            $collection = [];
            foreach ($tweets as $tweet) {
                $collection[] = Tweet::populateFromArray($tweet);
            }

            return $collection;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @param $tweets
     * @throws UsernameNotFoundException
     */
    private function assertUsernameNotFound($tweets)
    {
        if (is_array($tweets) && array_key_exists("errors", $tweets)) {
            throw new UsernameNotFoundException('Username not found');
        }
    }
}