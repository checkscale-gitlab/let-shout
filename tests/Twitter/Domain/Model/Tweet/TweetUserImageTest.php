<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:51
 */

namespace App\Tests\Twitter\Domain\Model;

use App\Twitter\Domain\Model\Tweet\TweetUserImage;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class TweetUserImageTest extends TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \DomainException
     */
    public function testShouldNotCreateWithEmptyString()
    {
        new TweetUserImage('');
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedIfItUrlImageIsInvalid()
    {
        $text = $this->faker->sentence;
        new TweetUserImage($text);
    }

    public function testGetValueShouldReturnTheUrlImage()
    {
        $expected = $this->faker->imageUrl();
        $text = new TweetUserImage($expected);
        $this->assertEquals($expected, $text->getValue());
        $this->assertEquals($expected, (string) $text);
    }
}
