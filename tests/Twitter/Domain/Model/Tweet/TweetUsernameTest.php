<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:51
 */

namespace App\Tests\Twitter\Domain\Model;

use App\Twitter\Domain\Model\Tweet\TweetUsername;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class TweetUsernameTest extends TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \DomainException
     */
    public function testShouldNotCreateWithEmptyString()
    {
        new TweetUsername('');
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedIfItExceedsTheMaximumAllowed()
    {
        $text = $this->faker->paragraph(10);
        new TweetUsername($text);
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedIfItDoesNotMeetTheMinimumAllowed()
    {
        $text = 'ab';
        new TweetUsername($text);
    }

    /**
     * @expectedException \DomainException
     */
    public function testCanNotBeCreatedWithIllegalCharacters()
    {
        $text = 'almodobar-20';
        new TweetUsername($text);
    }

    public function testGetValueShouldReturnTheUsername()
    {
        $expected = 'myNewUsername';
        $text = new TweetUsername($expected);
        $this->assertEquals($expected, $text->getValue());
        $this->assertEquals($expected, (string) $text);
    }
}
