<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 31/01/18
 * Time: 16:00
 */

namespace App\Tests\Twitter\Infrastructure\Application\Service;

use App\Twitter\Infrastructure\Application\Service\ApiExchange;
use App\Twitter\Infrastructure\Application\Service\TwitterApiSettings;
use PHPUnit\Framework\TestCase;

class TwitterApiSettingsTest extends TestCase
{
    /**
     * @var TwitterApiSettings
     */
    protected $settings;

    protected function setUp()
    {
        $token = 'oAccessToken';
        $tokenSecret = 'oAccessTokenSecret';
        $key = 'cKey';
        $secret = 'cSecret';
        $url = 'url';

        $this->settings = new TwitterApiSettings($token, $tokenSecret, $key, $secret, $url);
    }

    public function propertiesProvider(){
        $class = new \ReflectionClass(TwitterApiSettings::class);
        return [
            [$class, 'oAccessToken'],
            [$class, 'oAccessTokenSecret'],
            [$class, 'cKey'],
            [$class, 'cSecret'],
            [$class, 'url'],
        ];
    }

    public function arrayKeysProvider(){
        return [
            ['oauth_access_token'],
            ['oauth_access_token_secret'],
            ['consumer_key'],
            ['consumer_secret'],
        ];
    }

    public function testsExistsClassTwitterApiSettings()
    {
        $this->assertTrue(class_exists(get_class($this->settings)));
    }

    public function testsInstanceOfApiExchange()
    {
        $this->assertInstanceOf(ApiExchange::class, $this->settings);
    }

    /**
     * @param $attributeExpected \ReflectionClass
     * @param $class \ReflectionClass
     * @dataProvider propertiesProvider
     * @throws \ReflectionException
     */
    public function testsServiceHasProperties($class, $attributeExpected)
    {
        $this->assertClassHasAttribute($attributeExpected, $class->getName());
    }

    /**
     * @param $keyExpected \ReflectionClass
     * @dataProvider arrayKeysProvider
     */
    public function testsServiceReturnSettings($keyExpected)
    {
        $settings = $this->settings->getSettings();

        $this->assertTrue(is_array($settings));
        $this->assertArrayHasKey($keyExpected, $settings);
    }

    public function testsServiceReturnUrlBase()
    {
        $this->assertEquals('url', $this->settings->getUrlBase());
    }
}
