#!/bin/bash


buildDocker(){
    docker-compose up -d --build site
    echo -ne '\n' | docker-compose exec site bash
}

deployBackend(){
    currentPath=$(pwd)
    composer update
    cd $currentPath
}

deployFrontend(){
    currentPath=$(pwd)
    cd assets
    npm install
    ng build
    cd $currentPath
}

executeTests(){
    ./vendor/bin/phpunit
}


buildDocker
deployBackend
deployFrontend
executeTests

echo "----------------"
echo "DEPLOY FINISHED \n"
echo "Go to http://letshout.com \n"
echo "Enjoy :) \n"
