import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs';

import {of} from 'rxjs/observable/of';

import {
    debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import {ApiService} from '../service/api.service';
import {AlertService} from '../service/alert.service'
import {Tweet} from '../domain/model/tweet';

@Component({
    selector: 'app-root',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {

    @Input() tweet: Tweet;

    tweetForm: FormGroup;

    username: string;
    limit: number;

    searchResults: Array<Tweet>;
    sub: Subscription;

    constructor(private apiService: ApiService, private alertService: AlertService) {
    }

    ngOnInit() {
        this.tweetForm = new FormGroup ({
            username: new FormControl('', [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(15)
            ]),
            limit: new FormControl('',[Validators.required, Validators.pattern('[0-9]')])
        })
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    // Push a search term into the observable stream.
    onSubmit(): void {
        if(this.tweetForm.valid) {
            /* Any API call logic via services goes here */
            this.apiService.searchTweetes(this.username, this.limit).subscribe(
                data => {
                    if(data){
                        this.searchResults = data;
                    }else {
                        this.alertService.error("Not found");
                    }
                },
                error => {
                    this.alertService.error(error);
                }
            );
        }else{
            var error = this.tweetForm.getError('username');
            this.alertService.error(error);
        }
    }

}
