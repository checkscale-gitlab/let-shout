import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {SearchComponent} from './search/search.component';
import { AlertComponent } from './directives/alert/alert.component';

import {AlertService} from './service/alert.service';
import {ApiService} from './service/api.service';


@NgModule({
    declarations: [
        SearchComponent,
        AlertComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
    ],
    providers: [
        ApiService,
        AlertService
    ],
    bootstrap: [SearchComponent]
})
export class AppModule {
}
